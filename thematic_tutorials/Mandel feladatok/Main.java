package Mandelbrot;

public class Main {

    public static void main(String[] args) {
        Student Mihaly = new Student("Mihály", "I6I2PU");
        Lecturer kisPista = new Lecturer("Kis Pista");
        Subject prog2 = new Subject("Magasszintű programozási nyelvek 2", "INBPM0315-17", 6, kisPista, SubjectType.OBLIGATORY);
        Course prog2Labor = new Course("INBPM0315L-06", kisPista, "Szerda: 16:00-18:00", 18, prog2, CourseType.LABORATORY);
        kisPista.addCourse(prog2Labor);
        Mihaly.addCourse(prog2Labor);
        System.out.println("Felvett tárgy neve: " + Mihaly.getCourses().get(0).getSubject().getName());
        System.out.println("Kurzuskód: " + Mihaly.getCourses().get(0).getCourseId());
        System.out.println("Tárgy kreditszáma: " + prog2Labor.getSubject().getCredit());
    }
}
