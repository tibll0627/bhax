package Mandelbrot;

import java.util.ArrayList;
import java.util.List;

public class Lecturer {

    private String name;
    private List<Course> courses = new ArrayList<Course>();

    public Lecturer(String name) {
        this.name = name;
    }

    public String getName() {
        return name;
    }

    public void addCourse(Course course) {
        this.courses.add(course);
    }

    public void removeCourse(Course course) {
        this.courses.remove(course);
    }
}
