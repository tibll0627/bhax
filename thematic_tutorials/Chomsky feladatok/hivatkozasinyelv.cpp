#include <stdio.h>

int main(){
    
    //C89-ben nem fordul le C99-ben igen
    
    for(int i=0;i<5;i++);
    
    //Mindkettőben lefordul 
    int j;
    for(j=0;j<5;j++);
    
    //komment ami nem fordul le c89-ben

    /*komment ami lefordul c89-ben*/
    
return 0;
}

